import React from 'react';
import {View, ActivityIndicator} from 'react-native';

export default () => (
  <View style={{justifyContent: 'center', marginTop: 30}}>
    <ActivityIndicator size="large" />
  </View>
);
