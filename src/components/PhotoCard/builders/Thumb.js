import React from 'react';
import {Image} from 'react-native';

export default ({thumb}) => (
  <Image style={{width: 100, height: 100}} source={{uri: thumb}} />
);
