import React from 'react';
import {StyleSheet, View, TouchableOpacity} from 'react-native';
import Description from './builders/Description';
import Thumb from './builders/Thumb';
import Author from './builders/Author';

const style = StyleSheet.create({
  cardContainer: {
    flex: 1,
    flexDirection: 'row',
    margin: 10,
    borderWidth: 2,
    borderColor: 'black',
    overflow: 'hidden',
  },
  descView: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    marginLeft: 10,
    marginRight: 50,
  },
});

export default ({item, onPress}) => {
  return (
    <TouchableOpacity
      onPress={() => onPress(item.urls.full)}
      style={style.cardContainer}>
      <Thumb thumb={item.urls.thumb} />
      <View style={style.descView}>
        <Description text={item.description} />
        <Author author={item.user.name} />
      </View>
    </TouchableOpacity>
  );
};
