import React from 'react';
import {Provider} from 'react-redux';
import {configureStore} from './store';
import Navigator from './Navigator';
import {useScreens} from 'react-native-screens';

useScreens();

const store = configureStore();

const App = () => {
  return (
    <Provider store={store}>
      <Navigator />
    </Provider>
  );
};

export default App;
