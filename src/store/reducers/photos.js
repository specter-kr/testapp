import {
  LOAD_PHOTOS_REQUEST,
  LOAD_PHOTOS_SUCCESS,
  LOAD_PHOTOS_ERROR,
} from '../actionTypes';

const initialState = {isLoading: false, images: [], error: {}};

export const photos = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_PHOTOS_REQUEST:
      return {...state, isLoading: true};

    case LOAD_PHOTOS_SUCCESS:
      return {...state, isLoading: false, images: action.payload};

    case LOAD_PHOTOS_ERROR:
      return {...state, isLoading: false, error: action.payload};

    default:
      return state;
  }
};
