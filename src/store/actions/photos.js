import {
  LOAD_PHOTOS_REQUEST,
  LOAD_PHOTOS_SUCCESS,
  LOAD_PHOTOS_ERROR,
} from '../actionTypes';
import {unsplash} from '../../utils/unsplash';
import {toJson} from 'unsplash-js';

const startRequest = () => ({
  type: LOAD_PHOTOS_REQUEST,
});

const savePhotos = photos => ({
  type: LOAD_PHOTOS_SUCCESS,
  payload: photos,
});

const saveError = error => ({
  type: LOAD_PHOTOS_ERROR,
  payload: error,
});

export const loadPhotos = () => dispatch => {
  dispatch(startRequest());
  return unsplash.photos
    .listPhotos(1, 10, 'latest')
    .then(toJson)
    .then(json => {
      dispatch(savePhotos(json));
    })
    .catch(error => dispatch(saveError(error)));
};
