import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Photos from './screens/Photos';
import FullPhoto from './screens/FullPhoto';

const AppNavigator = createStackNavigator(
  {
    Photos: {
      screen: Photos,
      navigationOptions: () => ({
        title: 'Фотографии',
        headerTintColor: 'black',
      }),
    },
    FullPhoto: {
      screen: FullPhoto,
      navigationOptions: () => ({
        title: 'Полный размер',
        headerTintColor: 'black',
      }),
    },
  },
  {
    initialRouteName: 'Photos',
  },
);

export default createAppContainer(AppNavigator);
