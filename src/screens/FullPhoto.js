import React, {useState} from 'react';
import {View, Image} from 'react-native';
import Spinner from '../components/Spinner';

export default ({navigation}) => {
  let [loading, setLoading] = useState(false);
  const fullUrl = navigation.getParam('fullUri');
  return (
    <View>
      {loading ? <Spinner /> : null}
      <Image
        onLoadStart={() => setLoading(true)}
        onLoadEnd={() => setLoading(false)}
        style={{width: '100%', height: '100%'}}
        source={{uri: fullUrl}}
      />
    </View>
  );
};
