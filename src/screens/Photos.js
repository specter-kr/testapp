import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {loadPhotos} from '../store/actions/photos';
import {FlatList, SafeAreaView} from 'react-native';
import PhotoCard from '../components/PhotoCard';
import Spinner from '../components/Spinner';

const Photos = ({photos, loadPhotos, navigation}) => {
  useEffect(() => {
    const fetchPhotos = async () => await loadPhotos();
    fetchPhotos();
  }, [loadPhotos]);
  const {isLoading, images, error} = photos;
  const handlePress = full => {
    navigation.navigate('FullPhoto', {fullUri: full});
  };

  return (
    <SafeAreaView>
      {/* TODO showing error*/}
      {isLoading ? (
        <Spinner />
      ) : images.length > 0 ? (
        <FlatList
          data={images}
          renderItem={({item}) => {
            return <PhotoCard onPress={handlePress} item={item} />;
          }}
          keyExtractor={item => item.id}
        />
      ) : null}
    </SafeAreaView>
  );
};

const mapStateToProps = state => {
  return {
    photos: state.photos,
  };
};
const mapDispatchToProps = {
  loadPhotos,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Photos);
